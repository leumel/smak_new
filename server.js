const PORT = process.env.PORT || 5000
const Express = require("express")
const bodyParser = require("body-parser")
const exphbs = require("express-handlebars")
const app = Express()
const server = require("http").Server(app)
const path = require("path")

const indexRouter = require("./routes/index")


app.engine("handlebars", exphbs())
app.set("view engine", "handlebars")

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use("/assets", Express.static(path.join(__dirname, "public/assets")))
app.use("/styles", Express.static(path.join(__dirname, "public/styles")))

app.use("/",indexRouter)

app.use("*", function (req, res) {
    res.status(404).send("not found")
})

server.listen(PORT, function () {
    console.log(`Server started at ${PORT}`)
})